# create symlink
# ln -s ~/path/dotfiles/.file ~/.file

# IDE
ln -sf ~/.config/dotfiles/ide/.tmux.conf ~/.tmux.conf

# shell
ln -sf ~/.config/dotfiles/shell/.bash_profile ~/.bash_profile
ln -sf ~/.config/dotfiles/shell/.bashrc ~/.bashrc
ln -sf ~/.config/dotfiles/shell/.zshrc ~/.zshrc
#ln -sf ~/.config/dotfiles/shell/.zprofile ~/.zprofile
ln -sf ~/.config/dotfiles/shell/.dir_colors ~/.dir_colors
ln -sf ~/.config/dotfiles/shell/config.fish ~/.config/fish/config.fish

# gestures
ln -sf ~/.config/dotfiles/gestures/touchegg.conf ~/.config/touchegg/touchegg.conf

# git
ln -sf ~/.config/dotfiles/git/.gitconfig ~/.gitconfig

# dell
ln -sf ~/.config/dotfiles/dell/.i8kmon ~/.i8kmon

# kitty
ln -sf ~/.config/dotfiles/terminal/kitty.conf ~/.config/kitty/kitty.conf
ln -sf ~/.config/dotfiles/terminal/font.conf ~/.config/kitty/font.conf

# system
sudo ln -sf ~/.config/dotfiles/system/auto-cpufreq.conf /etc/auto-cpufreq.conf
