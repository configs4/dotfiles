# Use powerline

USE_POWERLINE="true"

source "/home/bvd/.config/dotfiles/shell/.path_dir"
source "/home/bvd/.config/dotfiles/shell/.env_variables"
source "/home/bvd/.config/dotfiles/shell/.pyenv"
source "/home/bvd/.config/dotfiles/shell/.aliases"
source "/home/bvd/.config/dotfiles/shell/.work-aliases"

# Source manjaro-zsh-configuration
if [[ -e /usr/share/zsh/manjaro-zsh-config ]]; then
  source "/usr/share/zsh/manjaro-zsh-config"
else
  export ZSH="$HOME/.oh-my-zsh"
  # ZSH_THEME="robbyrussell"
  ZSH_THEME="jreese"
  plugins=(git)
  source $ZSH/oh-my-zsh.sh
fi
# Use manjaro zsh prompt
if [[ -e /usr/share/zsh/manjaro-zsh-prompt ]]; then
  source "/usr/share/zsh/manjaro-zsh-prompt"
fi

eval "$(direnv hook zsh)"
