# ~/.config/fish/config.fish

starship init fish | source

if status is-interactive
    # Commands to run in interactive sessions can go here
end

source "/home/bvd/.config/dotfiles/shell/.path_dir"
# source "/home/bvd/.config/dotfiles/shell/.pyenv"
source "/home/bvd/.config/dotfiles/shell/.aliases"
source "/home/bvd/.config/dotfiles/shell/.env_variables"
source "/home/bvd/.config/dotfiles/shell/.work-aliases"
if test -f "/home/bvd/.config/dotfiles/shell/.api_keys"
  source "/home/bvd/.config/dotfiles/shell/.api_keys"
end

direnv hook fish | source
